FROM docker.io/python:3.11

LABEL version="0.2.0"
LABEL description="ROCK-IT container for Bluesky/Queueserver"
LABEL author="Devin Burke"
LABEL author.email="devin.burke@desy.de"
LABEL license="GNU GPL v2"

ENV POETRY_VIRTUALENVS_CREATE=false
ENV NO_INSTALL=true

RUN pip install --upgrade pip
RUN pip install poetry

RUN mkdir /logs
RUN mkdir /config
RUN mkdir -p /opt/bluesky
RUN mkdir /data
RUN mkdir /tests
RUN mkdir /startup

COPY src/bluesky /opt/bluesky
COPY config /config
COPY startup /startup
COPY tests /tests

# RUN pip install /opt/bluesky
RUN cd /opt/bluesky; poetry install
RUN chmod +x startup/startup.sh

VOLUME logs
VOLUME data
EXPOSE 60615
EXPOSE 60625

CMD ["startup/startup.sh"]