import os
import asyncio
import nxarray
import matplotlib

from bluesky import RunEngine
from bluesky.callbacks.best_effort import BestEffortCallback  # noqa
from bluesky.callbacks.zmq import Publisher
from bluesky.callbacks.broker import BrokerCallbackBase
from bluesky.utils import ProgressBarManager
import bluesky.plan_stubs as bps

from bluesky_queueserver import is_ipython_mode

from databroker.v1 import temp

from default_beamline.devices import *
from default_beamline.devices.device_init import create_devices, DEVICE_INIT_TIMEOUT, get_device_list
from default_beamline.plans import *


# matplotlib.use('Agg')

BLUESKY_DIR = os.environ.get('BLUESKY_DIR', os.getcwd())

if not BLUESKY_DIR.endswith('/'):
    BLUESKY_DIR = BLUESKY_DIR + '/'

DEVICE_FILE = os.environ.get('DEVICE_FILE', 'devices.yml')
DEVICE_URI_PATH = os.environ.get('DEVICE_URI_PATH', '/config/bluesky/devices/' + DEVICE_FILE)
    
DEVICE_INIT_TIMEOUT = 10


if __name__ == "__main__":
    
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    
    catalog = temp()
    bec = BestEffortCallback()
    device_list = get_device_list(DEVICE_URI_PATH)
    publisher = Publisher('bluesky_blissdata:5577')

    if is_ipython_mode():
        print("Starting RunEngine...")
        try:
            RE = RunEngine()
            RE.subscribe(bec)
            RE.subscribe(catalog.v1.insert)
            RE.subscribe(publisher)
            RE.waiting_hook = ProgressBarManager()
        except Exception as e:
            print(f"Error: {e}")
        print("RunEngine started")

        try:
            asyncio.run(create_devices(device_list, namespace=globals()))
        except Exception as e:
            print(f"Error: {e}")

    else:
        # In an async context, devices need to be created first, then the loop must
        # be passed to the RunEngine
        try:
            loop.run_until_complete(create_devices(device_list, namespace=globals()))
        except Exception as e:
            print(f"Error: {e}")

        print("Starting RunEngine...")
        try:
            RE = RunEngine(loop=loop)
            RE.subscribe(catalog.v1.insert)
            RE.subscribe(publisher)
        except Exception as e:
            print(f"Error: {e}")
        print("RunEngine started")
