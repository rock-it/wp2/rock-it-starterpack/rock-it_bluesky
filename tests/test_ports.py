import socket
import pytest
import os


@pytest.fixture
def qserver_host():
    return os.getenv('QSERVER_HOST', 'localhost')


@pytest.fixture
def qserver_ports():
    p1 = int(os.getenv('QSERVER_ZMQ_CONTROL_PORT', 60615))
    p2 = int(os.getenv('QSERVER_ZMQ_INFO_PORT', 60625))
    return [p1, p2]


@pytest.fixture
def redis_host():
    return os.getenv('REDIS_HOST', 'localhost')


@pytest.fixture
def redis_port():
    return int(os.getenv('REDIS_PORT', 6379))


def is_port_open(host, port):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        return s.connect_ex((host, port)) == 0


def test_redis_port(redis_host, redis_port):
    print(f'redis_host: {redis_host}')
    print(f'redis_port: {redis_port}')
    assert is_port_open(redis_host, redis_port)


def test_qserver_ports(qserver_host, qserver_ports):
    print(f'qserver_host: {qserver_host}')
    print(f'qserver_ports: {qserver_ports}')
    for port in qserver_ports:
        assert is_port_open(qserver_host, port)
