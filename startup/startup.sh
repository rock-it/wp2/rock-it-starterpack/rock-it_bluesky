#!/bin/bash

# If these environment variables are not set, set them to default values. These
# values are using in the Bluesky Queueserver configuration file.
export BLUESKY_DIR=${BLUESKY_DIR:-"/opt/bluesky"}
export NO_INSTALL=${NO_INSTALL:-"false"}
export REDIS_HOST=${REDIS_HOST:-"localhost"}
export REDIS_PORT=${REDIS_PORT:-6379}

# If BLUESKY_DIR is not /opt/bluesky, create a symbolic link to /opt/bluesky
if [ "${BLUESKY_DIR}" != "/opt/bluesky" ]; then
    ln -s ${BLUESKY_DIR} /opt/bluesky
fi

# VERBOSITY can be set to DEBUG, WARNING, or SILENT. If VERBOSITY is one of these three 
# values, set the --verbose, --quiet, or --silent flags, respectively.
if [ "${VERBOSITY^^}" == "DEBUG" ]; then
    export VERBOSITY_FLAG="--verbose"
elif [ "${VERBOSITY^^}" == "WARNING" ]; then
    export VERBOSITY_FLAG="--quiet"
elif [ "${VERBOSITY^^}" == "SILENT" ]; then
    export VERBOSITY_FLAG="--silent"
else
    export VERBOSITY_FLAG=
fi

evaluate_no_install() {
    local no_install_value="${1,,}"  # Convert to lowercase for consistency

    if [[ "$no_install_value" == "true" || "$no_install_value" == "True" || "$no_install_value" == "0" ]]; then
        return 0  # True
    elif [[ "$no_install_value" == "false" || "$no_install_value" == "False" || "$no_install_value" == "1" ]]; then
        return 1  # False
    else
        echo "Invalid value for NO_INSTALL: $no_install_value"
        return 1  # Default to false for invalid values
    fi
}

if evaluate_no_install "$NO_INSTALL"; then
    # Instead, append the bluesky directory to PYTHONPATH
    export PYTHONPATH=${BLUESKY_DIR}:${PYTHONPATH}
else
    # Install the beamline python package
    # If requirements.txt exists, install the packages listed in the file.
    echo "$(date) - Installing beamline python package requirements" | tee -a /logs/qserver.log
    if [ -f "${BLUESKY_DIR}/requirements.txt" ]; then
        pip install -r ${BLUESKY_DIR}/requirements.txt 1>/dev/null 2>>/logs/qserver.log
    fi
    # poetry install ${BLUESKY_DIR} 1>/dev/null 2>>/logs/qserver.log
    pip install ${BLUESKY_DIR}/. 1>/dev/null 2>>/logs/qserver.log
fi

# Start the Bluesky Queueserver
echo "$(date) - Starting Bluesky Queueserver" | tee -a /logs/qserver.log
echo "$(date) - Debug Mode: ${VERBOSITY_FLAG}" | tee -a /logs/qserver.log
start-re-manager --config="/config/qserver/config.yml" --ipython-dir="/config/qserver/.ipython" ${VERBOSITY_FLAG} | tee -a /logs/qserver.log

# Ensures the container runs forever
# tail -f /dev/null