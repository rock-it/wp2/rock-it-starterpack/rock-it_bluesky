import zmq
import pickle

#Get keyword args from command line
import argparse
parser = argparse.ArgumentParser(description='ZMQ client')
parser.add_argument('host', type=str, help='Host address', default='localhost')
parser.add_argument('port', type=int, help='Port number', default=5578)
parser.add_argument('publisher', type=str, help='Publisher name', default='bluesky-kernel')

host = parser.parse_args().host
port = parser.parse_args().port
publisher = parser.parse_args().publisher

# Publisher can be 'bluesky-kernel' or 'bluesky-queueserver'
# Queueserver default port is 60625
# Kernel has no default port but I use 5578

# Create a ZMQ context
context = zmq.Context()

# Create a ZMQ SUB socket
socket = context.socket(zmq.SUB)

# Connect to the server (replace with your server's address)
socket.connect(f"tcp://{host}:{port}")

# Subscribe to all topics
socket.setsockopt_string(zmq.SUBSCRIBE, '')

# print("Waiting for message...")
while True:
    try:
        # Receive a message
        message = socket.recv()

        if publisher == 'bluesky-kernel':
            # Split the message into prefix, name, and serialized doc
            prefix, name, serialized_doc = message.split(b' ', 2)

            # Decode the name from bytes to string
            name = name.decode()

            # Deserialize the document
            doc = pickle.loads(serialized_doc)

            # Print the name and document
            print(name, doc)

        elif publisher == 'bluesky-queueserver':
            # Expect utf-8
            print(message.decode())

    except Exception as e:
        print(f"Error occurred: {e}")
