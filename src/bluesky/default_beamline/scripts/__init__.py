"""
Utility functions for the default_beamline package
"""
import os
from typing import Dict
from .parse_yml import parse_yml


def get_device_list(device_uri_path: str = None) -> Dict[str, Dict[str, str]]:
    print("Loading device list...")
    dlist = {}
    if os.path.isfile(device_uri_path):
        dlist = parse_yml(*['devices'], yml_file=device_uri_path)
        print(f"Device list loaded from {device_uri_path}")
    else:
        print(f"Error: {device_uri_path} not found")
    return dlist


def is_running_in_ipython() -> bool:
    try:
        # noinspection PyUnresolvedReferences
        _ = get_ipython
        return True
    except NameError:
        return False
