"""
This module contains the device definitions for the default_beamline package.
Import all devices with::

    from default_beamline.devices import *

Devices:
    - VmMotor
    - VcCounter
    - TangoMover

"""

from ophyd_async.epics.demo import Sensor, SensorGroup, SampleStage

__all__ = ['Sensor',
           'SensorGroup',
           'SampleStage']
